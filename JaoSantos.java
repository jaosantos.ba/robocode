package JaoSantos;
import robocode.*;
import java.awt.Color;

//import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * JaoSantos - a robot by (José Antonio de Oliveira dos Santos)
 */
public class JaoSantos extends Robot
{
	/**
	 * run: JaoSantos's default behavior
	 */
	public void run() {
		// Initialization of the robot should be put here

		// After trying out your robot, try uncommenting the import at the top,
		// and the next line:

		// setColors(Color.red,Color.blue,Color.green); // body,gun,radar
		setColors(Color.blue,Color.white,Color.red);

		// Robot main loop
		while(true) {
			// Replace the next 4 lines with any behavior you would like
			double a = 100;
			double b = 100;
			
			ahead(a * 1.2);
			turnLeft(a / 4);
			turnGunRight(a * 3.6);
			
			back(b - 10);
			turnGunLeft(b * 1.8);
			
			ahead(a / 2);
			turnGunRight(360);
			
			back(b - 65);
			turnGunRight(a - 10);
		}
	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		// Replace the next line with any behavior you would like
		fire(3);
		turnGunRight(360);
		fire(2);
	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		// Replace the next line with any behavior you would like
		back(25);
		fire(1);		
		ahead(150);
	}
	
	/**
	 * onHitWall: What to do when you hit a wall
	 */
	public void onHitWall(HitWallEvent e) {
		// Replace the next line with any behavior you would like
		double x = getX();
		double y = getY();
		double m = getBattleFieldWidth();
		double n = getBattleFieldHeight();
		
		if((x == m)||(x == 0)) {
			ahead(35);
		}
		if((y == n)||(y == 0)) {
			back(60);
		}
	}
	
	public void onHitRobot (HitRobotEvent e) {
		back(10);
		turnGunRight(360);
		fire(1);
	}	
}
